"""create users table

Revision ID: 01d344cf9a53
Revises: f49103d6497c
Create Date: 2018-01-18 16:42:47.388786

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '01d344cf9a53'
down_revision = 'f49103d6497c'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
