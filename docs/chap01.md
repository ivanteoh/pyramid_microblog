# Chapter 1: Setup Pyramid

We are using Python 3 to create a virtual environment.

```
$ python3 -m venv venv
```

Activate the environment.

```
$ source venv/bin/activate
(venv) $ _
```

We are going to use cookiecutter for creating a Pyramid project using SQLite for persistent storage, SQLAlchemy for an ORM, URL dispatch for routing, and Jinja2 for templating.

```
(venv) $ pip install cookiecutter
(venv) $ cookiecutter https://github.com/Pylons/pyramid-cookiecutter-alchemy
```

Enter 'Pyramid Microblog' when 'project_name [Pyramid Scaffold]' question is presented.

Press 'Enter' to accept the default value of 'repo_name [pyramid_microblog]' question.

Change to the project directory.

```
(venv) $ cd pyramid_microblog 
```

Upgrade the packaging tools.

```
(venv) $ pip install --upgrade pip setuptools
```

Edit 'setup.py' to install python package 'alembic'. Python package 'alembic' is a database migration tool for SQLAlchemy.

```
requires = [
    ...,
    'alembic',
]
```

Install the project and its testing requirements, which includes pyramid, SQLAlchemy and Jinja2.

```
(venv) $ pip install -e ".[testing]"
```

Initialize alembic for the project.

```
(venv) $ alembic init alembic
```

Update 'sqlalchemy.url' value in ' alembic.ini' to the same value as in 'development.ini' file.

```
sqlalchemy.url = sqlite:///%(here)s/pyramid_microblog.sqlite
```

Configure 'alembic' to autogenerate migrations by updating the code below in 'alembic/env.py' file.

```
from pyramid_test.models.meta import Base
target_metadata = Base.metadata
```

Generate migrations.

```
(venv) $ alembic revision --autogenerate -m "Initial Commit"
```

Apply alembic migrations to upgrade the database.

```
(venv) $ alembic upgrade head
```

Verify the database is setup correctly using 'pshell' and the sample code in 'scripts/initializedb.py' script.

```
(venv) $ pshell development.ini
>>> url = registry.settings['sqlalchemy.url']
>>> from sqlalchemy import create_engine
>>> engine = create_engine(url)
>>> from pyramid_microblog.models.mymodel import MyModel
>>> from pyramid_microblog.models import get_session_factory
>>> from pyramid_microblog.models import get_tm_session
>>> session_factory = get_session_factory(engine)
>>> import transaction
>>> with transaction.manager:
...   dbsession = get_tm_session(session_factory, transaction.manager)
...   model = MyModel(name='one', value=1)
...   dbsession.add(model)
...
>>> with transaction.manager:
...   dbsession = get_tm_session(session_factory, transaction.manager)
...   mymodel = dbsession.query(MyModel).one()
...   print(mymodel)
...   print(mymodel.name) 
...
<pyramid_microblog.models.mymodel.MyModel object at 0x104bb0b38>
one
>>> exit()
```

Run the server.

```
(venv) $ pserve --reload development.ini
```

Go to 'http://localhost:6543' to see the web site.

Press 'ctrl-C' to stop the server.

## References

- https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
- https://docs.pylonsproject.org/projects/pyramid-blogr/en/latest/
- https://avolkov.github.io/pyramid-web-app-setup-with-with-postgres-sqlalchemy-and-alembic.html
