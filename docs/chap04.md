# Chapter 3: Database

- Create user model in `user.py`.

- Run this command below to generate db migration script.

```
(venv) $ alembic revision --autogenerate -m "create users table"
```

- Create an authentication form class.

- Create an authentication template.

- Create an authentication view.

- Create an authentication route.

