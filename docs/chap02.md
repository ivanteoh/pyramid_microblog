# Chapter 2: Views

We are going to simplify the base template, which is 'layout.jinja2'.

```
<body>
...

    <div class="starter-template">
      <div class="container">
        <div class="row">
          {% block content %}
            <p>No content</p>
          {% endblock content %}
        </div>

```

Create some variables to be used in Jinja2 template.

Add some conditions and loops on the template.

Add the navigation back to the home page.
