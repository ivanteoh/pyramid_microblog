# Chapter 3: Login form

Using wtforms to create login form.

- Add 'wtforms' under 'requires' in 'setup.py' file.

- Run this command below to install the package.

```
(venv) $ pip install -e ".[testing]"
```

- Create an authentication form class.

- Create an authentication template.

- Create an authentication view.

- Create an authentication route.

