# -*- coding: utf-8 -*-
"""Unit Tests."""
import unittest
import transaction

from pyramid import testing


def dummy_request(dbsession):
    """Dummy request for testing."""
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    """Base test."""

    def setUp(self):
        """Set up unit test."""
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('.models')
        settings = self.config.get_settings()

        from .models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        """Initial the database."""
        from .models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        """Tear down unit test."""
        from .models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)


class TestMyViewSuccessCondition(BaseTest):
    """Pass view test cases."""

    def setUp(self):
        """Set up unit test."""
        super(TestMyViewSuccessCondition, self).setUp()
        self.init_database()

        from .models import MyModel

        model = MyModel(name='one', value=55)
        self.session.add(model)

    def test_passing_view(self):
        """Pass view test."""
        from .views.default import my_view
        info = my_view(dummy_request(self.session))
        self.assertEqual(info['one'].name, 'one')
        self.assertEqual(info['project'], 'Pyramid Microblog')


class TestMyViewFailureCondition(BaseTest):
    """Fail view test cases."""

    def test_failing_view(self):
        """Fail view test."""
        from .views.default import my_view
        info = my_view(dummy_request(self.session))
        self.assertEqual(info.status_int, 500)
