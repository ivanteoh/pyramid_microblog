# -*- coding: utf-8 -*-
"""All forms."""
from wtforms import Form
from wtforms import PasswordField
from wtforms import StringField
from wtforms import SubmitField
from wtforms import validators

strip_filter = lambda x: x.strip() if x else None


class AuthForm(Form):
    """Auth form."""
    username = StringField(
        'Username',
        [validators.Length(min=1, max=255)],
        filters=[strip_filter])
    password = PasswordField(
        'Password',
        [validators.Length(min=3)])
