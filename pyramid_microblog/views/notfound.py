# -*- coding: utf-8 -*-
"""Not found view."""
from pyramid.view import notfound_view_config


@notfound_view_config(renderer='../templates/404.jinja2')
def notfound_view(request):
    """404 view."""
    request.response.status = 404
    return {}
