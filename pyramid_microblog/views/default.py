# -*- coding: utf-8 -*-
"""General views."""
from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config

from ..forms import AuthForm


@view_config(route_name='home', renderer='../templates/home.jinja2')
def my_view(request):
    """General view."""
    user = {'username': 'Miguel'}
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        }
    ]
    return {'title': 'Home', 'user': user, 'posts': posts}


@view_config(route_name='auth', renderer='../templates/auth.jinja2')
def auth_view(request):
    """Auth view."""
    form = AuthForm(request.POST)
    if request.method == 'POST' and form.validate():
        request.session.flash('Login requested for user {}'.format(
            form.username.data))
        return HTTPFound(location=request.route_url('home'))
    return {'form': form}
