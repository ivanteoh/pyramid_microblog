# -*- coding: utf-8 -*-
"""All routes."""

def includeme(config):
    """Include these route configs."""
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('auth', '/login')
