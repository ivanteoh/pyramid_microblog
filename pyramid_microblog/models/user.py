# -*- coding: utf-8 -*-
"""User model."""
import datetime

from sqlalchemy import (
    Column,
    DateTime,
    Index,
    Integer,
    String,
    Unicode
)

from .meta import Base


class User(Base):
    """User model."""
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255), index=True, unique=True, nullable=False)
    email = Column(String(120), index=True, unique=True)
    password_hash = Column(String(128))
    last_logged = Column(DateTime, default=datetime.datetime.utcnow)
